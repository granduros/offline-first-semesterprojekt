import React, { Component } from "react";
import "./App.css";
import AddTodo from "./components/addTodo/AddTodo";
import TodoList from "./components/todoList/TodoList";
import "bootstrap/dist/css/bootstrap.min.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClipboardList } from "@fortawesome/free-solid-svg-icons";
import DB from "./db";
import NetworkDetector from "./components/offlineNotification/NetworkDetector";
// React
// App.js on the other hand has the root component of the react app because every view and component are handled with hierarchy in React, where <App /> is the top most component in hierarchy.
// This gives you feel that you maintain hierarchy in your code starting from App.js.

// js. src/App. js : This is the file for App Component.
// App Component is the main component in React which acts as a container for all other components.
// src/App.css : This is the CSS file corresponding to App Component.
// How to install react app.
// 1. npm install -g create-react-app
// 2. npx create-react-app myApp
// 3. cd first-react-app
// 4. npm start

export const DbContext = React.createContext({});
export const ThemeContext = React.createContext({
  darkMode: {},
  toggleDarkMode: () => {},
});

class App extends Component {
  constructor() {
    super();
    this.state = {
      db: new DB("localdb", this),
      todos: [],
      darkMode: JSON.parse(localStorage.getItem("darkMode")),
    };
  }

  componentDidMount() {
    this.updateTodos();
  }

  updateTodos = async () => {
    let todos = await this.state.db.getAllDocuments();
    this.setState({
      todos,
    });
  };

  toggleDarkMode = () => {
    let darkMode = !this.state.darkMode;
    localStorage.setItem("darkMode", darkMode);
    this.setState({
      darkMode,
    });
  };

  render() {
    return (
      // Here className is used to point to a CSS class. <h1>ToDo App</h1> is just normal html Syntax.
      <div
        className={
          this.state.darkMode ? "dark-mode-background" : "ligh-mode-background"
        }
      >
        <div className="container">
          <div className="row">
            <div className="mx-auto">
              <div className="editheader">
                <h1 className="text-center title-top">
                  <FontAwesomeIcon
                    icon={faClipboardList}
                    color="#FCD344"
                    size="1x"
                  />
                  <label className="todo">ToDo</label>
                </h1>
              </div>
              {/* AddTodo imported from components/AddTodo folder and calling the class JSX*/}
              <DbContext.Provider value={this.state.db}>
                <ThemeContext.Provider
                  value={{
                    darkMode: this.state.darkMode,
                    toggleDarkMode: this.toggleDarkMode,
                  }}
                >
                  <AddTodo />
                </ThemeContext.Provider>
              </DbContext.Provider>
              {/* TodoList imported from components/TodoList folder and calling the class JSX*/}
              <DbContext.Provider value={this.state.db}>
                <ThemeContext.Provider
                  value={{
                    darkMode: this.state.darkMode,
                    toggleDarkMode: this.toggleDarkMode,
                  }}
                >
                  <TodoList todos={this.state.todos}></TodoList>
                </ThemeContext.Provider>
              </DbContext.Provider>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default NetworkDetector(App);
