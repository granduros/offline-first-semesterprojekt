import PouchDB from 'pouchdb';

class DB {
  constructor(name, app) {
    this.db = new PouchDB(name);
    this.app = app;
    this.hund = 'hund';
    const userName = '71b0a750-654b-43ac-af2e-0f0087cf8837-bluemix';
    const password =
      '79021f79a1e60b531a7ef8139526214a4d2c9fe9b99e878ae854f69be2f81b37';

    let encoded = window.btoa(`${userName}:${password}`);
    let auth = `Basic ${encoded}`;
    this.remotedb = new PouchDB(
      'https://71b0a750-654b-43ac-af2e-0f0087cf8837-bluemix.cloudant.com/todos',
      {
        fetch: function (url, opts) {
          opts.headers.append('Authorization', auth);
          return PouchDB.fetch(url, opts);
        },
      }
    );

    let opts = { live: true, retry: true };
    this.db
      .sync(this.remotedb, opts)
      .on('change', function (info) {
        console.log(`Change: ${info}`);
      })
      .on('paused', function (err) {
        console.log(`Paused: ${err}`);
      })
      .on('active', function () {
        console.log('Active');
      })
      .on('denied', function (err) {
        console.log(`Denied: ${err}`);
      })
      .on('complete', function (info) {
        console.log(`Completed: ${info}`);
      })
      .on('error', function (err) {
        console.log(`Error: ${err}`);
      })
      .on('change', function (change) {
        console.log('test done');
      });
  }

  getAllDocuments = async () => {
    let result = await this.db.allDocs({
      include_docs: true,
      attachments: true,
    });

    let todos = result.rows;

    return todos;
  };

  addDocument = (doc) => {
    this.db
      .put(doc)
      .then((response) => {
        if (doc.attachment) {
          this.addAttachment(response.rev, doc);
        }
        this.app.updateTodos();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  updateDocument = (doc) => {
    const db = this.db;
    const app = this.app;
    db.get(doc._id)
      .then(function (d) {
        return db.put({
          _id: doc._id,
          _rev: d._rev,
          title: doc.title,
          description: doc.description,
          dateCreated: doc.dateCreated,
          reminder: doc.reminder,
          attachment: doc.attachment,
        });
      })
      .then(function (response) {
        app.updateTodos();
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  updateDone = (docId, doneSwitch) => {
    const db = this.db;
    const app = this.app;

    db.get(docId)
      .then(function (doc) {
        return db.put({
          _id: doc._id,
          _rev: doc._rev,
          title: doc.title,
          description: doc.description,
          done: doneSwitch,
          dateCreated: doc.dateCreated,
          reminder: doc.reminder,
          attachment: doc.attachment,
        });
      })
      .then(function (response) {
        app.updateTodos();
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  deleteDocument = (docId) => {
    const db = this.db;
    const app = this.app;
    db.get(docId)
      .then(function (doc) {
        return db.remove(doc);
      })
      .then(function (result) {
        console.log(result);
        app.updateTodos();
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  addAttachment = (rev, doc) => {
    let attachment = btoa(doc.attachment);
    this.db
      .putAttachment(doc._id, 'image', rev, attachment, 'image/jpeg')
      .then((result) => {
        console.log(result);
      })
      .catch((err) => {
        console.log(err);
      });
  };
}

export default DB;
