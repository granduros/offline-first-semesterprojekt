import React, { useState, useContext } from "react";
import "./Style.css";
import AddModal from "../modals/AddModal";
import { Button, ButtonToolbar, Form } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle, faSun, faMoon } from "@fortawesome/free-solid-svg-icons";
import { ThemeContext } from "../../App";

function AddTodo() {
  const [addModal, setAddModal] = useState(false);
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);

  const toggleAddModal = () => {
    setAddModal(false);
  };

  return (
    <div
      className={`createbox ${
        darkMode ? "dark-mode-createbox" : "light-mode-createbox"
      }`}
    >
      <ButtonToolbar>
        {/* Add button, when click it, a AddModal will open*/}
        <Button
          variant="link"
          className="addbtn"
          onClick={() => setAddModal(true)}
        >
          <FontAwesomeIcon
            icon={faPlusCircle}
            className="btnCreate"
            color="#28a745"
            size="2x"
          />
        </Button>
        {/*AddModal popup takes */}
        <AddModal isAddModalOpen={addModal} toggleAddModal={toggleAddModal} />
        <h4 className="addtodo">Add todo</h4>
      </ButtonToolbar>

      <div className="toggle-container">
        <span style={{ color: darkMode ? "grey" : "yellow" }}>
          <FontAwesomeIcon icon={faSun} />
        </span>
        <span className="toggle">
          <Form>
            <Form.Check
              checked={darkMode}
              type="switch"
              id="custom-darkModeSwitch"
              label=""
              onChange={toggleDarkMode}
            />
          </Form>
          <label htmlFor="checkbox" />
        </span>
        <span style={{ color: darkMode ? "slateblue" : "grey" }}>
          <FontAwesomeIcon icon={faMoon} />
        </span>
      </div>
    </div>
  );
}
export default AddTodo;
