import React from "react";
import Todo from "../todo/Todo";
import "bootstrap/dist/css/bootstrap.min.css";
import { Container, Row, Col, CardColumns } from "react-bootstrap";

// TodoList that have all these parameter
// Showing a todoList

function TodoList(props) {
  const { todos } = props;

  return (
    <Row>
      <CardColumns>
        {todos.map((todo) => {
          return (
            <Col key={todo.doc._id}>
              <Todo todo={todo} />
            </Col>
          );
        })}
      </CardColumns>
    </Row>
  );
}

export default TodoList;
