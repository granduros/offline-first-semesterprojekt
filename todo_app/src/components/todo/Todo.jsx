import React, { useState, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import "./style.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTrashAlt,
  faEdit,
  faBell,
  faCheckCircle,
  faTimesCircle,
  faClock,
} from "@fortawesome/free-solid-svg-icons";
import "bootstrap/dist/css/bootstrap.min.css";
import { Card, CardColumns } from "react-bootstrap";
import "react-datepicker/dist/react-datepicker.css";
import DeleteModal from "../modals/DeleteModal";
import Details from "../modals/Details";
import EditModal from "../modals/EditModal";
import { DbContext, ThemeContext } from "../../App";

// Todo that have all these parameter
// When a user Add a todo, it wil be box with all these parameters
function Todo(props) {
  const db = useContext(DbContext);
  const { darkMode } = useContext(ThemeContext);

  const {
    _id,
    title,
    description,
    done,
    dateCreated,
    reminder,
    attachment,
  } = props.todo.doc;

  const [deleteModal, setDeleteModal] = useState(false);
  const [detailsModal, setDetailsModal] = useState(false);
  const [editModal, setEditModal] = useState(false);

  const handleDone = (e) => {
    let isDoneChecked = e.target.checked;
    db.updateDone(_id, isDoneChecked);
  };

  // Event handler - delete modal
  const toggleDeleteModal = () => {
    setDeleteModal(false);
  };

  // Event handler - details modal
  const toggleDetailsModal = () => {
    setDetailsModal(false);
  };

  // Event handler - edit modal
  const toggleEditModal = () => {
    setEditModal(false);
  };
  const isImageTrue = attachment;
  return (
    <React.Fragment>
      <Card
        style={{ width: "18rem" }}
        className={darkMode ? "dark-mode-card" : "light-mode-card"}
      >
        {isImageTrue ? (
          <Card.Img
            variant="top"
            onClick={() => setDetailsModal(true)}
            className="todoimg"
            height="180"
            src={attachment}
          />
        ) : (
          ""
        )}

        <Card.Body>
          <Card.Title className="click" onClick={() => setDetailsModal(true)}>
            <div className={darkMode ? "dark-mode-text" : "light-mode-text"}>
              {title}
            </div>
          </Card.Title>
          <Card.Text
            style={{
              overflow: "hidden",
              whiteSpace: "nowrap",
              textOverflow: "ellipsis",
            }}
          >
            {description} ...
          </Card.Text>
          {reminder ? (
            <label className="reminder" onClick={() => setEditModal(true)}>
              <FontAwesomeIcon
                className="bell"
                color="#ffc107"
                icon={faBell}
                size="1x"
              />
              {new Date(reminder).toLocaleDateString()}
            </label>
          ) : (
            <label className="icon reminder" onClick={() => setEditModal(true)}>
              <FontAwesomeIcon
                className="bell"
                color="#ffc107"
                icon={faClock}
                size="1x"
              />
              remind me!
            </label>
          )}
          <div className="done">
            <FontAwesomeIcon icon={faTimesCircle} color="red" size="1x" />
            <label>
              <Form className="doneSwitch">
                <Form.Check
                  type="switch"
                  id={props.todo.doc._id}
                  label=""
                  checked={done}
                  onChange={handleDone}
                />
              </Form>
            </label>
            <FontAwesomeIcon icon={faCheckCircle} color="green" size="1x" />
          </div>
          <hr></hr>
          <Button
            className="btnEdit"
            type="submit"
            value=""
            variant="info"
            onClick={() => setEditModal(true)}
          >
            <FontAwesomeIcon icon={faEdit} size="1x" />
          </Button>
          <Button
            className="btnDelete"
            type="submit"
            value=""
            variant="danger"
            onClick={() => setDeleteModal(true)}
          >
            <FontAwesomeIcon icon={faTrashAlt} size="1x" />
          </Button>
          <label className="right-side">
            Created: <div className="down">{dateCreated}</div>
          </label>
        </Card.Body>
      </Card>

      {/* DeleteModal imported from modals/modals folder and calling the class JSX*/}
      <DeleteModal
        toggleDeleteModal={toggleDeleteModal}
        isDeleteModalOpen={deleteModal}
        todoId={_id}
      />
      <Details
        isDetailsOpen={detailsModal}
        toggleDetails={toggleDetailsModal}
        todo={props.todo}
      />
      {/* EditModal imported from components/modals folder and calling the class JSX*/}
      <EditModal
        isEditModalOpen={editModal}
        toggleEditModal={toggleEditModal}
        todo={props.todo}
      />
    </React.Fragment>
  );
}

export default Todo;
