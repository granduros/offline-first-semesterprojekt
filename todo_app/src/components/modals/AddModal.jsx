import React, { useState, useContext, Fragment } from "react";
import { Modal, Form, Button, FormGroup, FormLabel } from "react-bootstrap";
import { DbContext, ThemeContext } from "../../App";
import uuid from "uuid/v4";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import "./AddModalStyle.css";
import { text } from "@fortawesome/fontawesome-svg-core";
// AddModal class
// This class can only be used, if a user want to add a new Todo

function AddModal(props) {
  // These values that has been assigned to const like:
  // handleChange: Type something
  // handleSubmit: When the form is submited
  // open: When the modal is open
  // hide: When click the close button to cose the modal
  const db = useContext(DbContext);
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);

  const { isAddModalOpen, toggleAddModal } = props;

  const initialState = {
    _id: uuid(),
    title: "",
    description: "",
    done: false,
    dateCreated: new Date().toLocaleDateString(),
    reminder: false,
    attachment: "",
  };

  const [todo, setTodo] = useState(initialState);
  const [fileUpload, setFileUpload] = useState(true);
  const [fileUploadName, setFileUploadName] = useState("Chosse file");
  const handleSubmit = (e) => {
    e.preventDefault();
    toggleAddModal();
    db.addDocument(todo);
    setTodo({ ...initialState });
  };

  const handleFiles = (e) => {
    const selectedFile = e.target.files[0];
    const selectedFileName = e.target.files[0].name;
    const reader = new FileReader();
    reader.addEventListener(
      "load",
      () => {
        setFileUpload(true);
        setFileUploadName(selectedFileName);
        setTodo({ ...todo, attachment: reader.result });
      },
      false
    );
    selectedFile && selectedFile.size < 600000
      ? reader.readAsDataURL(selectedFile)
      : setFileUpload(false);
  };

  return (
    <Modal
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      show={isAddModalOpen}
      centered
    >
      <div
        className={
          darkMode
            ? "dark-mode-modal-background"
            : "light-mode-modal-background"
        }
      >
        <Modal.Header closeButton onClick={toggleAddModal}>
          <Modal.Title id="contained-modal-title-vcenter">
            <FontAwesomeIcon
              className={darkMode ? "dark-mode-icon" : "light-mode-icon"}
              icon={faPlusCircle}
              size="1x"
            />{" "}
            Create ToDo
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <FormGroup id="formAddName">
              <Form.Label className="label">Title</Form.Label>
              <Form.Control
                className={darkMode ? "dark-mode-input" : "light-mode-input"}
                required
                value={todo.title}
                type="text"
                name="title"
                maxLength={20}
                placeholder="Enter title"
                onChange={(e) => setTodo({ ...todo, title: e.target.value })}
              />
            </FormGroup>
            <FormGroup id="formAddDescription">
              <Form.Label className="label">Description</Form.Label>
              <Form.Control
                className={
                  darkMode ? "dark-mode-textarea" : "light-mode-textarea"
                }
                required
                value={todo.description}
                as="textarea"
                name="description"
                rows="3"
                placeholder="Enter description"
                onChange={(e) =>
                  setTodo({ ...todo, description: e.target.value })
                }
              />
            </FormGroup>
            <FormGroup>
              <FormLabel className="label">File upload</FormLabel>
              <Fragment>
                <div className="custom-file">
                  <Form.File custom id="formcheck-api-regular">
                    {!fileUpload && <Form.File.Input isInvalid />}
                    <Form.File.Label data-browse="Upload image">
                      {fileUploadName}
                    </Form.File.Label>
                    <Form.Control.Feedback type="invalid">
                      The file is too big!
                    </Form.Control.Feedback>
                    <Form.File.Input onChange={handleFiles} />
                  </Form.File>
                </div>
              </Fragment>
            </FormGroup>
            <Button type="submit" variant="success">
              Create
            </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={toggleAddModal}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
}
export default AddModal;
