import React, { useContext } from "react";
import { Modal, Button } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { DbContext, ThemeContext } from "../../App";
import "./deleteModalStyle.css";
// This is an confirmbox class, that will show on delete Modal before deleting.
//A functional component is just a plain JavaScript function which accepts props as an argument and returns a React element.
// Functional component are much easier to read and test because they are plain JavaScript functions without state or lifecycle-hooks.

function DeleteModal(props) {
  const db = useContext(DbContext);
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);
  const { isDeleteModalOpen, toggleDeleteModal, todoId } = props;

  const handleDelete = () => {
    db.deleteDocument(todoId);
    toggleDeleteModal();
  };

  // Her we return a datele Modal like an cofirmbox
  return (
    <Modal show={isDeleteModalOpen} onHide={toggleDeleteModal} variant="danger">
      <div
        className={
          darkMode
            ? "dark-mode-modal-background"
            : "light-mode-modal-background"
        }
      >
        <Modal.Header closeButton></Modal.Header>
        <Modal.Header className="d-flex flex-column align-items-center">
          <div className="text-center mb-3 mt-3">
            <FontAwesomeIcon
              className={darkMode ? "dark-mode-icon2" : "light-mode-icon2"}
              icon={faTrashAlt}
              size="4x"
            />
          </div>
          <Modal.Title className="text-center">
            <p>Are you sure?</p>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="des">
          Do you really want to delete this item? This process cannot be undone.
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleDelete}>
            Yes, delete it!
          </Button>
          <Button variant="secondary" onClick={toggleDeleteModal}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
}

const textColor = {
  color: "#721c24",
};

export default DeleteModal;
