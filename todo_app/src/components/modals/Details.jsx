import React, { useContext } from "react";
import { Modal, Button, ModalFooter, Image } from "react-bootstrap";
import "./details.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faInfoCircle,
  faCalendarAlt,
  faBell,
} from "@fortawesome/free-solid-svg-icons";
import { ThemeContext } from "../../App";
// This is an confirmbox class, that will show on delete Modal before deleting.
//A functional component is just a plain JavaScript function which accepts props as an argument and returns a React element.
// Functional component are much easier to read and test because they are plain JavaScript functions without state or lifecycle-hooks.

function Details(props) {
  const { isDetailsOpen, toggleDetails, todo } = props;
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);
  // Her we return a detail Modal, to show todo with more content.
  return (
    <Modal show={isDetailsOpen} onHide={toggleDetails}>
      <div
        className={
          darkMode
            ? "dark-mode-modal-background"
            : "light-mode-modal-background"
        }
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <FontAwesomeIcon
              className={darkMode ? "dark-mode-icon" : "light-mode-icon"}
              icon={faInfoCircle}
              size="1x"
            />{" "}
            Detail
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>{todo.doc.title}</h4>
          {todo.doc.attachment ? (
            <Image height="180" width="286" src={todo.doc.attachment} rounded />
          ) : (
            ""
          )}
          <p className="detailsDescription">{todo.doc.description}</p>
          <hr />
          <label>
            Reminder:{" "}
            {todo.doc.reminder ? (
              <label className="reminder">
                <FontAwesomeIcon
                  className="bell"
                  color="#ffc107"
                  icon={faBell}
                  size="1x"
                />
                {new Date(todo.doc.reminder).toLocaleDateString()}
              </label>
            ) : (
              "not set"
            )}
          </label>
          <label className="date">
            {" "}
            <FontAwesomeIcon icon={faCalendarAlt} size="1x" /> Created:{" "}
            {todo.doc.dateCreated}
          </label>
        </Modal.Body>
      </div>
    </Modal>
  );
}

export default Details;
