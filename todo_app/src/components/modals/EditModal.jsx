import React, { useState, useContext } from "react";
import {
  Modal,
  Form,
  Button,
  FormGroup,
  FormLabel,
  Image,
} from "react-bootstrap";
import "./EditModelStyle.css";
import { DbContext, ThemeContext } from "../../App";
import DatePicker from "react-datepicker";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit } from "@fortawesome/free-solid-svg-icons";

// EditModal class
// This class can only be used, if a user want to edit a created Todo.
function EditModal(props) {
  // render is a subclasse for react component.
  // render method is the only required method in a class component.
  // when it's going to be called, so it's examine this.props and this.state and returns one of the following:
  // React elements, Arrays and fragments, Portals, String and numbers or Booleans or null.

  const db = useContext(DbContext);
  const { darkMode, toggleDarkMode } = useContext(ThemeContext);
  const { isEditModalOpen, toggleEditModal, todo } = props;

  const initialState = {
    _id: todo.doc._id,
    title: todo.doc.title,
    description: todo.doc.description,
    dateCreated: todo.doc.dateCreated,
    reminder: new Date(todo.doc.reminder).toString(),
    attachment: todo.doc.attachment,
  };

  const [todoItem, setTodoItem] = useState(initialState);
  const [fileUpload, setFileUpload] = useState(true);
  const [fileUploadName, setFileUploadName] = useState("Chosse file");

  const handleSubmit = (e) => {
    e.preventDefault();
    toggleEditModal();
    db.updateDocument(todoItem);
  };

  const handleFiles = (e) => {
    const selectedFile = e.target.files[0];
    const selectedFileName = e.target.files[0].name;
    const reader = new FileReader();
    reader.addEventListener(
      "load",
      () => {
        setFileUpload(true);
        setFileUploadName(selectedFileName);
        setTodoItem({ ...todoItem, attachment: reader.result });
      },
      false
    );
    if (selectedFile && selectedFile.size < 600000) {
      reader.readAsDataURL(selectedFile);
    } else {
      setFileUpload(false);
      setTodoItem({ ...todoItem, attachment: reader.result });
    }
  };

  const handleClose = () => {
    setTodoItem({ ...initialState });
    toggleEditModal();
  };

  return (
    // Returns a Modal
    <Modal
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      show={isEditModalOpen}
      centered
    >
      <div
        className={
          darkMode
            ? "dark-mode-modal-background"
            : "light-mode-modal-background"
        }
      >
        <Modal.Header closeButton onClick={toggleEditModal}>
          <Modal.Title id="contained-modal-title-vcenter">
            <FontAwesomeIcon
              className={darkMode ? "dark-mode-icon" : "light-mode-icon"}
              icon={faEdit}
              size="1x"
            />{" "}
            Edit ToDo
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {/* Edit form  */}
          <Form onSubmit={handleSubmit}>
            {/*Title input*/}
            <FormGroup id="formAddName">
              <Form.Label className="label">Title</Form.Label>
              <Form.Control
                className={darkMode ? "dark-mode-input" : "light-mode-input"}
                value={todoItem.title}
                required
                type="text"
                name="title"
                placeholder="Enter title"
                onChange={(e) =>
                  setTodoItem({ ...todoItem, title: e.target.value })
                }
              />
            </FormGroup>

            {/*Description input*/}
            <FormGroup id="formAddDescription">
              <Form.Label className="label">Description</Form.Label>
              <Form.Control
                value={todoItem.description}
                className={
                  darkMode ? "dark-mode-textarea" : "light-mode-textarea"
                }
                required
                as="textarea"
                name="description"
                rows="3"
                placeholder="Enter description"
                onChange={(e) =>
                  setTodoItem({ ...todoItem, description: e.target.value })
                }
              />
            </FormGroup>
            <FormGroup>
              <FormLabel className="label">File upload</FormLabel>
              <div className="custom-file">
                <Form.File custom id="formcheck-api-regular">
                  {!fileUpload && <Form.File.Input isInvalid />}
                  <Form.File.Label data-browse="Button text">
                    {fileUploadName}
                  </Form.File.Label>
                  <Form.Control.Feedback type="invalid">
                    The file is too big man!
                  </Form.Control.Feedback>
                  <Form.File.Input onChange={handleFiles} />
                </Form.File>
                <Image
                  className="editImage"
                  src={todoItem.attachment}
                  rounded
                />
              </div>
            </FormGroup>
            {/*Reminder input*/}
            <FormGroup>
              <FormLabel className="label">Reminder </FormLabel>
              <div>
                <DatePicker
                  className={darkMode ? "dark-mode-input" : "light-mode-input"}
                  selected={new Date(todoItem.reminder)}
                  onChange={(e) => setTodoItem({ ...todoItem, reminder: e })}
                />
              </div>
            </FormGroup>
            {/*Submit button*/}
            <Button type="submit" variant="success">
              Save
            </Button>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </div>
    </Modal>
  );
}
export default EditModal;
